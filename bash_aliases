# Files and directories
alias ..='cd ..'
alias cd..='cd ..'
alias cp='cp -v'
alias mv='mv -nuv'
alias ls='ls --color=auto --group-directories-first'
alias lsd='ls -d */'

alias vcs='vcs -n 9 -c 3 -u nobody -j -H 200'

# tmux things
alias tmux='TERM=screen-256color-bce tmux -2'

# git and vim things
alias vimm='vim $(git ls-files -m)'

alias fd=fdfind

# Things that might not work for everyone
alias gomp="python3 $HOME/repositories/GOMP.git/gomp.py"

alias torbrowser='(cd $HOME/Software/tor-browser_en-US/Browser/ ; ./start-tor-browser)'
